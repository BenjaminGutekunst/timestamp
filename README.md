# README #

Just open the excel sheet and click "Beginn", so that you can track you time at work.
Optionally, the weekly working time and the break time can be set.

### Timekeeping ###

**Project Type:** Excel VBA Macro 

**Version:** 1.0.9

- Fix broken conditional formating caused by local formulas (argument separator ";"--> german, ","--> english)
- Update public holiday

**Version:** 1.1.0

- Helper Function WorksheetExists, removing repeated code
- Pressing of "Beginn" creates new Monat sheet if not exists, thus rendering "Neuer Monat" redundant
- Move new Worksheet at the end of all Monatssheets but before "Men�", "Urlaub"  and "Feiertage"
- 3 Shortcuts for Desktop: Beginn/Pause/Ende so that we do not need to open Excel every time